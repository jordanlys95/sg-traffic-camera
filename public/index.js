'use strict'

const MAPBOX_API = 'pk.eyJ1Ijoiam9yZGFubHlzOTUiLCJhIjoiY2thMjRoZjk5MDhkejNmbzV2NnQ3OGR2aCJ9.lKziTfam_bfJEQ43Eh3rig';
function initMap(token) {
    mapboxgl.accessToken = token;
    let _map = new mapboxgl.Map({
        container: 'map',
        center: [103.847, 1.339],
        zoom: 9.5,
        style: 'mapbox://styles/mapbox/dark-v10'
    });
    _map.addControl(new PitchControl(), 'bottom-left')
    _map.on('style.load', function () {
        createBBOX(_map);
        add3DBuildings(_map);
        addSpeedEnforcmentCameras(_map);
    })
    // _map.on('load', function () {

    // })
    _map.on('click', 'speed_enforcement_cameras', function (e) {
        var coordinates = e.features[0].geometry.coordinates.slice();
        var { name, type } = e.features[0].properties;

        // Ensure that if the map is zoomed out such that multiple
        // copies of the feature are visible, the popup appears
        // over the copy being pointed to.
        while (Math.abs(e.lngLat.lng - coordinates[0]) > 180) {
            coordinates[0] += e.lngLat.lng > coordinates[0] ? 360 : -360;
        }

        new mapboxgl.Popup()
            .setLngLat(coordinates)
            .setHTML(`
            <p>
                <strong>${name}</strong>
            </p>
            <p>
                ${type}
            </p>
        `)
            .addTo(_map);
    })
    return _map;
}

function createBBOX(_map) {
    const countryBBOX = {
        SGP: { sw: { lat: 1.156, lng: 103.565 }, ne: { lat: 1.475, lng: 104.13 } }
    }

    const { SGP } = countryBBOX;
    const bboxGeoJSON = turf.polygon([
        [
            [-180, 90],
            [180, 90],
            [180, -90],
            [-180, -90],
            [-180, 90],
        ],
        [
            [SGP.sw.lng, SGP.ne.lat],
            [SGP.ne.lng, SGP.ne.lat],
            [SGP.ne.lng, SGP.sw.lat],
            [SGP.sw.lng, SGP.sw.lat],
            [SGP.sw.lng, SGP.ne.lat],
        ]
    ]);

    _map.addLayer(
        {
            id: "bbox",
            type: "fill",
            source: {
                type: "geojson",
                tolerance: 10,
                buffer: 0,
                data: bboxGeoJSON,
            },
            paint: {
                "fill-color": "rgba(0,0,0,.5)",
                "fill-antialias": false,
            },
        }
    );
}

function add3DBuildings(_map) {
    console.log('Adding 3d Buildings...')
    _map.addLayer(
        {
            id: "3d-buildings",
            source: "composite",
            "source-layer": "building",
            filter: ["==", "extrude", "true"],
            type: "fill-extrusion",
            minzoom: 15,
            layout: {
                visibility: "none",
            },
            paint: {
                "fill-extrusion-color": "#aaa",
                // use an 'interpolate' expression to add a smooth transition effect to the
                // buildings as the user zooms in
                "fill-extrusion-height": ["get", "height"],
                "fill-extrusion-base": ["get", "min_height"],
                "fill-extrusion-opacity": 0.6,
            },
        }, getLastSymbolLayer(_map)
    );
}

function addSpeedEnforcmentCameras(_map) {
    console.log('Adding Speed Enforcement Cameras...')

    fetch('https://data.gov.sg/api/action/datastore_search?resource_id=ded670a7-5abd-4b81-b590-a9aa78774bdc')
        .then(body => body.json())
        .then(({ result }) => {
            console.log(result);
            let { records } = result;

            let speed_cam_geojson_features = records.map(record =>
                turf.point([record.location_longitude, record.location_latitude], {
                    name: record.location,
                    type: record.type_of_speed_camera
                })
            );

            _map.addSource('src-speed_enforcement_cameras', {
                type: 'geojson',
                data: turf.featureCollection(speed_cam_geojson_features)
            })

            _map.addLayer({
                id: 'speed_enforcement_cameras',
                source: 'src-speed_enforcement_cameras',
                type: 'circle',
                paint: {
                    'circle-pitch-alignment': 'map',
                    'circle-color': '#FF3D00',
                    'circle-stroke-color': '#fff',
                    'circle-stroke-width': 2
                },
                layout: {

                }
            }, getLastSymbolLayer(_map))

            // test 3d
            let speed_cam_geojson_circle_features = records.map(record =>
                turf.circle([record.location_longitude, record.location_latitude], 0.0001,
                    {
                        steps: 10,
                        units: 'kilometers',
                        properties: {
                            name: record.location,
                            type: record.type_of_speed_camera
                        }
                    }
                ));

            _map.addSource('src-circle-speed_enforcement_cameras', {
                type: 'geojson',
                data: turf.featureCollection(speed_cam_geojson_circle_features)
            })

            _map.addLayer({
                id: '3D-speed_enforcement_cameras',
                source: 'src-circle-speed_enforcement_cameras',
                type: 'fill-extrusion',
                paint: {
                    'fill-extrusion-base': 0,
                    'fill-extrusion-height': 4,
                    'fill-extrusion-color': '#FF3D00',
                },
                layout: {

                }
            }, getLastSymbolLayer(_map))
        })
}
function getLastSymbolLayer(_map) {
    // Get the layer beneath any symbol layer.
    var layers = _map.getStyle().layers;

    var labelLayerId;
    for (var i = 0; i < layers.length; i++) {
        if (layers[i].type === "symbol" && layers[i].layout["text-field"]) {
            labelLayerId = layers[i].id;
            break;
        }
    }
    return labelLayerId;
}
class PitchControl {
    onAdd(map) {
        this._map = map;
        const container = document.createElement('div');
        container.className = 'mapboxgl-ctrl mapboxgl-ctrl-group';
        container.innerHTML = '<button class="mapboxgl-ctrl-icon mapboxgl-ctrl-custom-pitch" type="button"><div>3D</div></button>';
        container.onclick = function () {
            var pitch = map.getPitch();
            var nextPitch = 0;
            if (pitch < 30) {
                nextPitch = 30;
            } else if (pitch < 45) {
                nextPitch = 45;
            } else if (pitch < 60) {
                nextPitch = 60;
            }
            map.easeTo({ pitch: nextPitch });
        };
        map.on('pitchend', this.onPitch.bind(this));
        this._container = container;
        return this._container;
    }
    onPitch() {
        const pitch = this._map.getPitch();
        // Custom 3D building feature when pitch is >0
        if (pitch === 0) {
            this._map.setLayoutProperty('3d-buildings', 'visibility', 'none');
        } else {
            this._map.setLayoutProperty('3d-buildings', 'visibility', 'visible');

        }
        this._container.classList.toggle('active', !!pitch);
        const text = this._container.getElementsByTagName('div')[0];
        text.style.transform = 'rotate3d(1,0,0,' + pitch + 'deg)';
    }
    onRemove() {
        this._container.parentNode.removeChild(this._container);
        this._map.off('pitchend', this.onPitch.bind(this));
        this._map = undefined;
    }
};

let MAP = initMap(MAPBOX_API);





